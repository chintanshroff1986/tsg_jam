﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunManager : MonoBehaviour
{
    public List <DebrisEmmitor> debrisEmmitors = new List<DebrisEmmitor>();
    public Transform DebrisEmmitor_pivot;
    public float EmittorRotateSpeed = 5f;


    public Sun sun;

    private void Update()
    {

        DebrisEmmitor_pivot.transform.Rotate(new Vector3(0, EmittorRotateSpeed * Time.deltaTime, 0));
    }


}
