﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debris : MonoBehaviour
{
    
    public float currentTime = 0f;
    public bool isBlack;

    private void OnEnable()
    {
        currentTime = 0f;
        float random = Random.Range(0.3f, 1.5f);
        transform.localScale = new Vector3(random, random,random);
    }

    private void Update()
    {
        MoveDebris();
        
    }

    public void MoveDebris()
    {

        // Smoothly move 
        //transform.localPosition = Vector3.Lerp(transform.localPosition, GameManager.Instance.player_manager.player_gameplay.transform.position, GameManager.Instance.sun_Manager.sun.debrisSpeed * Time.deltaTime);
        transform.localPosition += transform.forward * GameManager.Instance.sun_Manager.debrisEmmitors[0].debrisSpeed * Time.deltaTime;

        if (currentTime < GameManager.Instance.sun_Manager.debrisEmmitors[0].debris_lifeTime)
        {
            currentTime += Time.deltaTime;
        }
        else
        {
            Destroy();
        }
    }

    private void OnCollisionEnter(Collision other)
    {


        if (other.gameObject.tag == "Player")
        {
            Destroy();
            if (isBlack)
            {
                GameManager.Instance.player_manager.player_gameplay.Health -= 0.1f;
                GameManager.Instance.uiManager.UpdateHealth(GameManager.Instance.player_manager.player_gameplay.Health);
            }
            else
            {
                GameManager.Instance.player_manager.player_gameplay.UpdateBulllets(5);
            }
           
        }
        else if (other.gameObject.tag == "Bullet")
        {
            if (!other.gameObject.GetComponent<Bullet>().isYellowBullet)
            {
                Destroy();
                GameManager.Instance.player_manager.player_gameplay.UpdateBulllets(5);
            }
        }

    }

    //on debris hit
    public void Destroy()
    {
        SimplePool.Despawn(this.gameObject);
    }
}
