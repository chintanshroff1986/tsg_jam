﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;


public class UIManager : MonoBehaviour
{
    public GameObject GameOverScreen;
    public TMP_Text GameoverDesc_text;
    public Slider healthSlider;
    public TMP_Text score;
    public TMP_Text bullets;


    public void UpdateHealth(float health)
    {
        GameManager.Instance.uiManager.healthSlider.value = health;
        if (health <=0)
        {
            EndGame();
        }
    }

    public void TryAgain()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void EndGame()
    {
        GameManager.Instance.currentState = GameState.gameover;
        GameOverScreen.SetActive(true);
        //GameoverDesc_text.text = "You took too much damage and got destroyed";
    }


}
