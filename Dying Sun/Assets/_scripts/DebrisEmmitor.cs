﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisEmmitor : MonoBehaviour
{
    public Debris debris_black;
    public Debris debris_yellow;


    public float debrisSpeed = 0.2f;
    public float debris_lifeTime = 100f;

    public float debrisDelay = 1f;
    public float timeSinceLastDebris = 0f;


    private void Update()
    {
        if(GameManager.Instance.currentState == GameState.playing)
        {
            if (timeSinceLastDebris >= debrisDelay)
            {
                float r = Random.value;
                if (r >0.8f)
                {
                    SimplePool.Spawn(debris_yellow.gameObject, transform.position, transform.rotation);
                }
                else
                {
                    SimplePool.Spawn(debris_black.gameObject, transform.position, transform.rotation);
                }
               
                timeSinceLastDebris = 0;
            }
            else
            {
                timeSinceLastDebris += Time.deltaTime;
            }
        }
       
    }
}
