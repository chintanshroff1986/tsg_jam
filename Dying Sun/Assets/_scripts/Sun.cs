﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour
{

    public float sun_dyingSpeed = 2;
    public float maxSize = 8f;
    public float minSize = 1f;






    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.currentState == GameState.playing)
        {
            ChangeSize();
        }
    }

    public void ChangeSize()
    {
        if (transform.localScale.y >= 1f)
            transform.localScale -= new Vector3(.1f, 0.1f, 0.1f) * Time.deltaTime * sun_dyingSpeed;
        else
        {
            GameManager.Instance.uiManager.healthSlider.value = 0;
            GameManager.Instance.player_manager.player_gameplay.Health = 0;

            GameManager.Instance.uiManager.EndGame();
        }
    }

    private void OnCollisionEnter(Collision other)
    {


        if (other.gameObject.tag == "Bullet")
        {
            if (other.gameObject.GetComponent<Bullet>().isYellowBullet)
            {
                if (transform.localScale.x < maxSize)
                {
                    transform.localScale += new Vector3(.5f, 0.5f, 0.5f);
                }
                else
                {
                    transform.localScale = new Vector3(maxSize, maxSize, maxSize);

                }
            }

            else
            {
                if (transform.localScale.x > minSize)
                {
                    transform.localScale -= new Vector3(.5f, 0.5f, 0.5f);
                }
                else
                {
                    GameManager.Instance.uiManager.EndGame();
                    
                }



            }
        }

    }
}

