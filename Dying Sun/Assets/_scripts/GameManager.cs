﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum GameState { playing, gameover };

public class GameManager : MonoBehaviour
{
    public SunManager sun_Manager;
    public PlayerManager player_manager;
    public UIManager uiManager;
    public GameState currentState = GameState.playing;


    //singleton
    public static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("GameManager");
                    _instance = container.AddComponent<GameManager>();
                }
            }

            return _instance;
        }
    }
}
