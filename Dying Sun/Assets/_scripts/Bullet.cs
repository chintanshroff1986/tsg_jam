﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    // Update is called once per frame
    public Vector3 aimTarget;
    public bool isYellowBullet;

    private void Update()
    {
        MoveBullet();
    }


    public void MoveBullet()
    {
        // Smoothly move 
       transform.localPosition = Vector3.Lerp(transform.localPosition, GameManager.Instance.sun_Manager.sun.transform.position, GameManager.Instance.player_manager.player_gameplay.bulletSpeed * Time.deltaTime);
       //GetComponent<Rigidbody>().AddForce(aimTarget.normalized * -800f);
    }


    private void OnCollisionEnter(Collision other)
    {
       

        if (other.gameObject.tag == "Sun")
        {
            Destroy();
        }
        else if (other.gameObject.tag == "Debris")
        {
            Destroy();
        }
    }

    //on bullet hit
    public void Destroy()
    {
        SimplePool.Despawn(this.gameObject);
    }
}
